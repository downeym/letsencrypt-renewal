#!/usr/bin/env python3

import ssl, socket, argparse, subprocess, urllib.request, urllib.parse
from os import environ
from datetime import datetime
from dateutil.parser import parse as parse_datetime  # from python-dateutil
from dateutil.tz import tzutc

LETS_ENCRYPT_DIR = '/etc/letsencrypt/live'
CHAIN = 'fullchain.pem'
KEY = 'privkey.pem'
GITLAB_REQ = 'https://gitlab.com/api/v4/projects/{}/pages/domains/{}'


env_vars = [
    'CI_PROJECT_ID', 'GITLAB_USER_EMAIL',
    'CERTBOT_RENEWAL_PIPELINE_GIT_TOKEN'
    ]


def get_cert(hostname):
    context = ssl.create_default_context()
    connection = context.wrap_socket(socket.socket(socket.AF_INET), server_hostname=hostname)
    connection.connect((hostname, 443))
    return connection.getpeercert()


if __name__ == "__main__":
    # check env vars
    for var in env_vars:
        if var not in environ:
            print("Environment variable '{}' not found. Exiting.".format(var))
            exit(1)

    proj_id = environ['CI_PROJECT_ID']
    token = environ['CERTBOT_RENEWAL_PIPELINE_GIT_TOKEN']
    email = environ['GITLAB_USER_EMAIL']

    # Parse arguments
    aparser = argparse.ArgumentParser(description="Generate Let's Encrypt Cert.")
    aparser.add_argument('-n', '--hostname', action='append', required=True,
                         help="Name of hosts who's cert you want to generate or renew")
    aparser.add_argument('-t', '--time-to-expiry', default=30, type=int, help='Time in days left until cert expires')
    aparser.add_argument('-e', '--email', default=email)
    args = aparser.parse_args()

    # see if cert exists and has less that provide time-to-expiry
    print("Checking for existing cert...")
    cert = None
    try:
        cert = get_cert(args.hostname[0])
    except OSError:
        # can't pull cert, maybe SSL not active, like first time set-up, we'll keep going
        pass
    except ssl.CertificateError:
        # assume case where no cert set-up so is returning gitlab.io
        # add and continue
        pass

    if cert is not None:
        # Check if within the time-to-expiry
        print("Cert found. Checking expiry...")
        expiry = parse_datetime(cert['notAfter'])
        if (expiry - datetime.now(tzutc())).days > args.time_to_expiry:
            print("Cert for '"+args.hostname[0]+"' valid for more than provided time-to-expiry of " +
                  str(args.time_to_expiry) + " days.")
            exit(0)

    # now lets generate the cert!
    print("Cert expires sooner than selected expiry, regenerating...")
    certbot_args = ['certbot', 'certonly', '--manual', '--debug',
                    '--preferred-challenges=http', '-m', args.email,
                    '--agree-tos', '--manual-auth-hook', 'letsencrypt_authenticator.py', '--manual-cleanup-hook',
                    'letsencrypt_cleanup.py',
                    '--manual-public-ip-logging-ok']

    # a little list weirdness to handle multiple domain arguments
    for host in args.hostname:
        certbot_args.append('-d')
        certbot_args.append(host)

    cp = None
    try:
        cp = subprocess.run(certbot_args, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    except:
        print("Certbot failed to execute. Exiting.")
        if cp is not None:
            print('Error:\n{}\nOut:\n{}\n'.format(cp.stderr, cp.stdout))

        print('Log:\n')
        with open('/var/log/letsencrypt/letsencrypt.log', 'r') as f:
            for line in f:
                print(line)
        exit(5)

    # Set up the form data
    data = dict()
    with open(LETS_ENCRYPT_DIR + '/' + args.hostname[0] + '/' + CHAIN, 'r') as f:
        data['certificate'] = f.read()
    with open(LETS_ENCRYPT_DIR + '/' + args.hostname[0] + '/' + KEY, 'r') as f:
        data['key'] = f.read()

    data = urllib.parse.urlencode(data)
    data = data.encode('ascii')

    # post chain and key for each domain in the project
    for domain in args.hostname:
        print('Installing Cert for domain: ' + domain)
        req = urllib.request.Request(GITLAB_REQ.format(proj_id, domain), method='PUT', data=data)
        req.add_header('PRIVATE-TOKEN', token)
        try:
            with urllib.request.urlopen(req) as f:
                # could log results, but no need
                pass
        except:
            print('Could not install cert for domain: ' + domain)
            continue
        print('Cert installed/renewed for domain: ' + domain)
